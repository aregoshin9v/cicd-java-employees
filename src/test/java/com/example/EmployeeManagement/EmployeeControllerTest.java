package com.example.EmployeeManagement;

import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class EmployeeControllerTest {
    @Test
    public void testGetEmployeeEndpoint() {
        given()
                .contentType("application/json")
                .when()
                .get("http://localhost:8080/employee/6c3948c3-4f6f-4c5e-9420-7cf732ae674")
                .then()
                .assertThat()
                .body("response.message", equalTo("Пользователь с таким id не существует"));
    }

}
