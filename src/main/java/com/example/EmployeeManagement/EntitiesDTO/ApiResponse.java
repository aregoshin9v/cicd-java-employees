package com.example.EmployeeManagement.EntitiesDTO;

import com.example.EmployeeManagement.Entities.Employee;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ApiResponse {
    private Map<String, Object> response = new HashMap<>();

    public void setStatus(int status) {
        response.put("status", status);
    }

    public void setId(String id) {
        response.put("id", id);
    }

    public void setMessage(String message) {
        response.put("message", message);
    }

    public void setMembers(List<Employee> members) {
        response.put("members", members);
        response.put("totalElements", members.size());
    }

    public void setEmployee(Employee employee) {
        response.put("id", employee.getUuid());
        response.put("name", employee.getName());
        response.put("position", employee.getPosition());
    }

    public Map<String, Object> getResponse() {
        return response;
    }

    /*private Map<String, Object> toJson(Map<String, Object> map) {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.convertValue(map, Map.class);
    }*/
}
