package com.example.EmployeeManagement.EntitiesDTO;

import lombok.Data;

@Data
public class EmployeeDTO {
    private String uuid;
    private String name;
    private String position;
}