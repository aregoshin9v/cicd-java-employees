package com.example.EmployeeManagement.Mappers;

import com.example.EmployeeManagement.Entities.Employee;
import com.example.EmployeeManagement.EntitiesDTO.EmployeeDTO;
import com.example.EmployeeManagement.Enums.Position;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Slf4j
@Component
public class EmployeeMapper {
    public static Employee toEntity(EmployeeDTO dto) {
        Employee entity = new Employee();
        if (dto.getUuid() != null) {
            entity.setUuid(UUID.fromString(dto.getUuid()));
        }
        entity.setName(dto.getName());
        entity.setPosition(Position.valueOf(dto.getPosition()));

        return entity;
    }
}
