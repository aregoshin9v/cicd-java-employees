package com.example.EmployeeManagement;

import com.example.EmployeeManagement.Entities.Employee;
import com.example.EmployeeManagement.EntitiesDTO.ApiResponse;
import com.example.EmployeeManagement.EntitiesDTO.EmployeeDTO;
import com.example.EmployeeManagement.Enums.Position;
import com.example.EmployeeManagement.Mappers.EmployeeMapper;
import com.example.EmployeeManagement.Methods.UUIDValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@Slf4j
@RestController
public class EmployeeController {
    private final EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @PostMapping("/employee")
    public ApiResponse saveEmployee(@RequestBody EmployeeDTO employeeDTO) {
        Employee employee = EmployeeMapper.toEntity(employeeDTO);

        return employeeService.saveEmployee(employee);
        //return new ResponseEntity<>(savedEmployee, HttpStatus.CREATED);
        //return new ResponseEntity<>(employeeService.saveEmployee(employee), HttpStatus.CREATED);
    }

    @DeleteMapping("/employee/{id}")
    public ApiResponse deleteEmployeeById(@PathVariable String id,
                                          @RequestBody EmployeeDTO request,
                                          @RequestHeader("Authorization") String authorizationHeader) {

        ApiResponse response = new ApiResponse();

        if (!UUIDValidator.isValidUUID(id)) {
            response.setMessage("Некорректный формат UUID");
            return response;
        }

        if (request.getUuid() != null && !request.getUuid().equals(id))
        {
            if(employeeService.isAdministrator(UUID.fromString(id))){
                return employeeService.deleteEmployeeById(UUID.fromString(request.getUuid()));
            }
            else
            {
                response.setMessage("Недостаточно прав для выполнения операции");
                return response;
            }
        }

        return employeeService.deleteEmployeeById(UUID.fromString(id));
    }

    @GetMapping("/employees")
    public ApiResponse getAllEmployees(@RequestParam(required = false) String name,
                                       @RequestParam(required = false) String position) {
        if (name != null) {
            return employeeService.getEmployeesByName(name);
        }
        if (position != null) {
            return employeeService.getEmployeesByPosition(Position.valueOf(position));
        }

        return employeeService.getAllEmployees();
    }

    @GetMapping("/employee/{id}")
    public ApiResponse getEmployeeById(@PathVariable String id) {
        if (!UUIDValidator.isValidUUID(id)) {
            ApiResponse response = new ApiResponse();
            response.setMessage("Некорректный формат UUID");
            return response;
        }

        return employeeService.getEmployeeById(UUID.fromString(id));
    }

    @PutMapping("/employee/{id}")
    public ApiResponse updateEmployee(@PathVariable UUID id,
                                      @RequestBody EmployeeDTO request,
                                      @RequestHeader("ApplicationAuthorization") String authorizationHeader) {

        ApiResponse response = new ApiResponse();

        UUID authorizedEmployeeId = UUID.fromString(authorizationHeader);
        boolean isAuthorizedUserAdmin = employeeService.isAdministrator(authorizedEmployeeId);

        log.info(String.valueOf(authorizationHeader));

        if (!id.equals(authorizedEmployeeId) && !isAuthorizedUserAdmin) {
            response.setMessage("Недостаточно прав для выполнения операции");
            response.setStatus(HttpStatus.FORBIDDEN.value());
            return response;
        }

        String name = request.getName();
        String positionStr = request.getPosition();

        Position position = Position.valueOf(positionStr);

        response = employeeService.updateEmployee(id, name, position);

        return response;
    }

        /*@GetMapping("/name/{name}")
    public Employee getEmployeeByName(@PathVariable String name) {
        return employeeService.getEmployeeByName(name);
    }*/
}
