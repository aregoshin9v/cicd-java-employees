package com.example.EmployeeManagement.Enums;

public enum Position {
    Employee,
    Administrator,
    Deleted
}
