package com.example.EmployeeManagement;

import com.example.EmployeeManagement.Entities.Employee;
import com.example.EmployeeManagement.EntitiesDTO.ApiResponse;
import com.example.EmployeeManagement.Enums.Position;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Service
@AllArgsConstructor
public class EmployeeService {
    @Autowired
    private final EmployeeRepository employeeRepository;

    public ApiResponse saveEmployee(Employee employee) {
        ApiResponse response = new ApiResponse();

        if (employee.getUuid() != null && employeeRepository.existsByUuid(employee.getUuid())) {
            response.setStatus(HttpStatus.CONFLICT.value());
            response.setMessage("Пользователь с таким id уже существует");
            return response;
        }

        if (employee.getUuid() == null) {
            employee.setUuid(UUID.randomUUID());
        }
        employeeRepository.save(employee);

        response.setStatus(HttpStatus.CREATED.value());
        response.setId(employee.getUuid().toString());
        return response;
    }

    public ApiResponse deleteEmployeeById(UUID id) {
        ApiResponse response = new ApiResponse();

        Employee employee = employeeRepository.findByUuid(id);

        if(employee == null){
            response.setMessage("Пользователь с таким id не существует");
        }
        else {
            employee.setPosition(Position.Deleted);
            employeeRepository.save(employee);
            response.setEmployee(employee);
        }

        return response;
    }

    public ApiResponse getAllEmployees() {
        ApiResponse response = new ApiResponse();
        response.setMembers(employeeRepository.findAll());
        return response;
    }

    public ApiResponse getEmployeesByPosition(Position position) {
        ApiResponse response = new ApiResponse();
        response.setMembers(employeeRepository.findByPosition(position));
        return response;
    }

    public ApiResponse getEmployeesByName(String name) {
        ApiResponse response = new ApiResponse();
        response.setMembers(employeeRepository.findByNameContainingIgnoreCase(name));
        return response;
    }

    public ApiResponse getEmployeeById(UUID id) {
        ApiResponse response = new ApiResponse();

        Employee employee = employeeRepository.findByUuid(id);

        if(employee == null){
            response.setMessage("Пользователь с таким id не существует");
        }
        else {
            response.setEmployee(employee);
        }

        return response;
    }

    public Employee getEmployeeByName(String name) {
        return employeeRepository.findByName(name);
    }

    public ApiResponse updateEmployee(UUID id, String name, Position position) {
        ApiResponse response = new ApiResponse();

        Optional<Employee> optionalEmployee = Optional.ofNullable(employeeRepository.findByUuid(id));
        if (optionalEmployee.isPresent()) {
            Employee employee = optionalEmployee.get();

            if (name != null) {
                employee.setName(name);
            }
            if (position != null) {
                employee.setPosition(position);
            }

            employeeRepository.save(employee);

            response.setMessage("Сотрудник успешно обновлен");
            response.setStatus(HttpStatus.OK.value());
        } else {
            response.setMessage("Сотрудник с указанным id не найден");
            response.setStatus(HttpStatus.NOT_FOUND.value());
        }

        return response;
    }

    public boolean isAdministrator(UUID employeeId) {
        Optional<Employee> optionalEmployee = Optional.ofNullable(employeeRepository.findByUuid(employeeId));
        if (optionalEmployee.isPresent()) {
            Employee employee = optionalEmployee.get();

            return employee.getPosition() == Position.Administrator;
        }

        return false;
    }
}
