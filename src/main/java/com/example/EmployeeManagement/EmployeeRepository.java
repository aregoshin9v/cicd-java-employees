package com.example.EmployeeManagement;

import com.example.EmployeeManagement.Entities.Employee;
import com.example.EmployeeManagement.Enums.Position;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    Employee findByName(String name);
    boolean existsByUuid(UUID uuid);
    Employee findByUuid(UUID id);
    Employee deleteByUuid(UUID id);

    List<Employee> findByPosition(Position position);
    List<Employee> findByNameContainingIgnoreCase(String name);
}
