package com.example.EmployeeManagement.Entities;

import com.example.EmployeeManagement.Enums.Position;
import jakarta.persistence.*;
import lombok.Data;

import java.util.UUID;

@Entity
@Data
@Table(name = "employee")
public class Employee {
    @Id
    private UUID uuid;
    private String name;
    private Position position;
}