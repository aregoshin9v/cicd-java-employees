FROM openjdk:21.0.2
ADD /target/EmployeeManagement-0.0.1-SNAPSHOT.jar backend.jar
ENTRYPOINT ["java", "-jar", "backend.jar"]